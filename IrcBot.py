from twisted.words.protocols import irc
from twisted.internet import reactor, protocol, defer
from twisted.python import log
from lxml import html
import BotActions
import ConfigParser
import sys, time, re, requests

class MessageLogger:
    # Class taken from VossV2 https://gitlab.dicgaming.net/Theowningone/voss
    def log(self, message):
        self.doLog('',message)

    def action(self, message):
        self.doLog('* ',message)

    def info(self, message):
        self.doLog('~~~ ',message)

    def meta(self, message):
        self.doLog('--- ',message)

    def doLog(self, prefix, message):
        timestamp = time.strftime("[%H:%M:%S]", time.localtime(time.time()))
        log.msg("%s%s" % (prefix, message))
        print '%s%s%s' % (timestamp, prefix, message)

class GrabberException(Exception):
    pass

class Grabber(object):
    # Class taken from Hoggy2 https://github.com/jeremysmitherman/Hoggy2
    buffer = []

    def stack(self, user, line):
        self.buffer.append((user, line))
        while len(self.buffer) > 100:
            del self.buffer[0]

    def grab(self, user, lines=1):
        quote_lines = []
        for x in reversed(self.buffer):
            if len(quote_lines) == lines:
                break

            if user == x[0]:
                quote_lines.append(x[1])

        if len(quote_lines):
            quote = ""
            quote_lines.reverse()
            quote += "<%s> " % user
            quote += " ".join(quote_lines)
            return quote
        else:
            raise GrabberException('No quotes found for user in last 100 lines.')

class IrcBot(irc.IRCClient):

    def __init__(self, config, *args, **kwargs):
        self.logger = MessageLogger()
        self.grabber = Grabber()
        self.config = config
        self.nickname = self.config.get('irc', 'nick')
        self.password = self.config.get('irc', 'password')
        self._namescallback = {}

    def names(self, channel):
        channel = channel.lower()
        d = defer.Deferred()
        if channel not in self._namescallback:
            self._namescallback[channel] = ([], [])

        self._namescallback[channel][0].append(d)
        self.sendLine("NAMES %s" % channel)
        return d

    def irc_RPL_NAMREPLY(self, prefix, params):
        channel = params[2].lower()
        nicklist = params[3].split(' ')

        if channel not in self._namescallback:
            return

        n = self._namescallback[channel][1]
        n += nicklist

    def irc_RPL_ENDOFNAMES(self, prefix, params):
        channel = params[1].lower()
        if channel not in self._namescallback:
            return

        callbacks, namelist = self._namescallback[channel]

        for cb in callbacks:
            cb.callback(namelist)

        del self._namescallback[channel]

    def connectionMade(self):
        irc.IRCClient.connectionMade(self)
        self.logger.meta('[Connection made at %s]' % time.asctime(time.localtime(time.time())))
 
    def connectionLost(self, reason):
        irc.IRCClient.connectionLost(self, reason)
        self.logger.meta("[Disconnected at %s] %s" % (time.asctime(time.localtime(time.time())), str(reason)))

    # callbacks for events
    def signedOn(self):
        #Called when bot has succesfully signed on to server.
        #TODO: make it possible to join multiple channels
        self.join(self.config.get('irc', 'channel'))
 
    def joined(self, channel):
        #This will get called when the bot joins the channel.
        self.logger.meta("[I have joined %s]" % channel)
 
    def privmsg(self, user, channel, msg):
        #This will get called when the bot receives a message.
        user = user.split('!', 1)[0]
        self.logger.log("<%s> %s" % (user, msg))

        response = None

        if user == "racerbot94":
            self.logger.meta("[Ignored that filthy race bot]")
            return

        if msg.startswith('!'):
            cmdarr = msg.split(' ')
            command = cmdarr[0].lower()
            args = cmdarr[1:]

            try:
                response = BotActions.Action.actions.get(command)().execute(self, user, channel, args)
            except TypeError, ex:
                self.logger.info(str(ex))
                # response = "I have never heard of such a preposterous command."
            except core.ActionException, ex:
                self.logger.info(str(ex))
                response = str(ex)
            except Exception, ex:
                self.logger.info(str(ex))
                response = "Unexpected exception: {0}".format(str(ex))

        elif msg.find("nospoil") == -1:
            imgext = (".png", ".jpg", ".jpeg", ".jif", ".jiff", ".gif", ".tif", ".tiff")
            # Web scraping happens below here. Starting with some regex called The Holy Grail!
            pattern = r"((([A-Za-z]{3,9}:(?:\/\/)?)(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+|(?:www\.|[\-;:&=\+\$,\w]+@)[A-Za-z0-9\.\-]+)((?:\/[\+~%\/\.\w\-_]*)?\??(?:[\-\+=&;%@\.\w_]*)#?(?:[\.\!\/\\\w]*))?)"
            regexObj = re.search(pattern, msg, re.I)
            if regexObj:
                url = regexObj.group()
                if not url.split('?')[0].endswith(imgext):
                    if url.find("http") == -1:  # request hates you if your url doesn't have http://
                        url = "http://" + url   # so let's add it if it's not there

                    try:
                        head = {
                            "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.76 Safari/537.36",
                            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
                            "Accept-Language": "en-US,en;q=0.8",
                            "Cache-Control": "max-age=0",
                            "Connection": "keep-alive"
                        }
                        page = requests.get(url, headers=head)
                    except requests.exceptions.HTTPError, ex:
                        self.logger.info("An exception occured: %s" % str(ex))
                        response = "Titl... Nope, something went wrong"
                    except requests.exceptions.TooManyRedirects, ex:
                        self.logger.info("An exception occured: %s" % str(ex))
                        response = "Titl... Nope, something went wrong"
                    except requests.exceptions.RequestException, ex:
                        self.logger.info("An exception occured: %s" % str(ex))
                        response = "Titl... Nope, something went wrong"
                    else:
                        tree = html.fromstring(page.text) # Get the html
                        title = tree.xpath("//title/text()")[0].strip() # Get the <title> and work some magic
                        if url.find("youtube.com/watch") != -1 or url.find("youtu.be") != -1: # Get view count as well if the url leads to youtube
                            views = tree.xpath("//div[@id='watch7-views-info']//div[@class='watch-view-count']/text()")[0].strip()
                            response = "Title: %s (Views: %s)" % (title, views)
                        else:
                            response = "Title: %s" % (title)#, url.split('/')[2])

        self.grabber.stack(user, msg)

        if response is not None:
            if channel == self.nickname:
                self.logger.log("%s - <%s> %s" % (channel, self.nickname, response.encode('utf-8')))
                self.msg(user, response.encode('utf-8'))
            else:
                self.logger.log("%s - <%s> %s" % (channel, self.nickname, response.encode('utf-8')))
                self.msg(channel, response.encode('utf-8'))

    def userJoined(self, user, channel):
        #Called when another user joins the channel
        user = user.split('!', 1)[0]

    def alterCollidedNick(self, nickname):
        return nickname+'_'
 
    def action(self, user, channel, msg):
        #This will get called when the bot sees someone do an action.
        user = user.split('!', 1)[0]

class IrcBotFactory(protocol.ClientFactory):
 
    def __init__(self, config):
        self.config = config
        self.channel = self.config.get('irc', 'channel')

    def buildProtocol(self, addr):
        p = IrcBot(self.config)
        p.factory = self
        return p
 
    def clientConnectionLost(self, connector, reason):
        #If we get disconnected, reconnect to server.
        print "Connection Lost: %s" % reason
        connector.connect()

    def clientConnectionFailed(self, connector, reason):
        print "Connection Failed: %s" % reason
        reactor.stop()

if __name__ == '__main__':
    config = ConfigParser.ConfigParser()
    config.read('config.ini')

    log.startLogging(open('lillapy.log'), setStdout=False)

    bot = IrcBotFactory(config)
    reactor.connectTCP(config.get('irc', 'host'), int(config.get('irc', 'port')), bot)
    reactor.run()