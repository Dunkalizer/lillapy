from abc import ABCMeta, abstractproperty, abstractmethod
from random import choice
import sys, requests, datetime, time
import sqlite3 as lite

"""Heavily based on Hoggy2's core actions https://github.com/jeremysmitherman/Hoggy2"""

class ActionException(Exception):
    def __init__(self, message):
        super(ActionException, self).__init__(message)

class Action(object):
    __metaclass__ = ABCMeta
    actions = {}

    @abstractproperty
    def shortdesc(self):
        return "No help available."

    @abstractproperty
    def longdesc(self):
        return "No help available."

    @abstractmethod
    def execute(self, bot, user, channel, args):
        pass

class ping(Action):
    
    def shortdesc(self):
        return "Check if the bot is listening"

    def longdesc(self):
        return "Sometimes he likes to ignore people, or just straight up go away."

    def execute(self, bot, user, channel, args):
        return choice(["Hoozin'd it up.  Naw Just kidding. Pong.", "pong", "pang", "poong", "ping?", "pop", "pa-pong!", "kill yourse- sorry, pong", "ta-ping!", "Wasn't that Mulan's fake name?"])

class CatFacts(Action):

    def shortdesc(self):
        return "Thank you for subscribing to catfacts!"

    def longdesc(self):
        return "Meow"

    def execute(self, bot, user, channel, args):
        r = requests.get("http://catfacts-api.appspot.com/api/facts")
        return unicode(r.json()["facts"][0])

class Slap(Action):

    def shortdesc(self):
        return "SLAP BET!"

    def longdesc(self):
        return "Slap someone with something. Use: !slap <user>"

    def execute(self, bot, user, channel, args):
        target = None
        if (len(args)):
            target = args[0]

        weapons = [
            "a large pit board",
            "a large trout",
            "a slightly moist purple mushroom",
            "a sperm whale"
        ]

        action = "\001ACTION slaps %s around a bit with %s\001"

        if target is None or target.lower() == bot.nickname.lower():
            return action % (user, choice(weapons))
        else:
            return action % (target, choice(weapons))

class UrbanDict(Action):

    def shortdesc(self):
        return "Don't user A-10"

    def longdesc(self):
        return "It's Urban Dictionary... You know what it's for. Use: !urban <search term>"

    def execute(self, bot, user, channel, args):
        try:
            r = requests.get("http://api.urbandictionary.com/v0/define?term=%s" % " ".join(args))
        except requests.exceptions.RequestException, ex:
            return str(ex)
        else:
            json = r.json()
            if not json['list']:
                return "I found nothing."
            else:
                return json['list'][0]['definition']

class Tell(Action):

    def shortdesc(self):
        return "Somtimes people aren't here."

    def longdesc(self):
        return "Need to talk to someone who is not online?. Use: !tell <user> <message>"

    def execute(self, bot, user, channel, args):
        if not args:
            return "You have to specify a user."
        target = args[0]
        print target
        bot.names(channel).addCallback(self.got_names, channel, target)

    def got_names(nicklist, channel, target):
        print 2
        # prefixes = ['~','&','@','%','+']

        # for (i,nick) in enumerate(nicklist):
        #     if nick[0] in prefixes:
        #         nicklist[i] = nick[1:]
        if target in nicklist:
            bot.logger.info("[Channel = %s -- Target = %s]" % (channel, user))

class Grab(Action):
    # Class taken from Hoggy2 https://github.com/jeremysmitherman/Hoggy2
    def shortdesc(self):
        return "Grab the last n lines of a specifc user and create a quote"

    def longdesc(self):
        return "Usage: !grab <user> <number of lines>  number of lines defaults to 1"

    def execute(self, bot, user, channel, args):
        if (len(args) == 1 or len(args) == 2) and args[1] == '':
            num_lines = 1
        else:
            try:
                num_lines = int(args[1])
            except:
                num_lines = 0

        if num_lines < 1:
            return "{0}... Don't be a dipshit.".format(user)

        if args[0].lower() == bot.nickname.lower():
            return "Leave me out of this"

        quote = bot.grabber.grab(args[0], num_lines)
        q = Quote()
        return q.execute(bot, user, channel, ["add", quote])
        # return quote

class Quote(Action):
    def shortdesc(self):
        return "Display or add quotes"

    def longdesc(self):
        return "With no arguments will display a random quote. [#] will display the quote with the specified ID. [add <quote>] will add the specified <quote> to the db. [search <string>] will look for that string in the db."

    def execute(self, bot, user, channel, args):
        if args[0].lower() == bot.nickname.lower():
            return "Leave me out of this"
        database = bot.config.get('bot', 'dbfile')
        admin = bot.config.get('bot', 'admin')
        # print database
        response = None

        try:
            con = lite.connect(database)

        except lite.Error, e:
            print "Error %s:" % e.args[0]
            sys.exit(1)

        else:
            cur = con.cursor()

            if len(args):
                mod = args[0] 
                #add quote
                if mod == "add":
                    quote = " ".join(args[1:])
                    t = datetime.datetime.now()
                    cur.execute("INSERT INTO quotes (quote, added_by, created_at) VALUES (?,?,?)", (quote, user, t))
                    lid = cur.lastrowid
                    bot.logger.meta("[Successfully added quote \"%s\" with id %s]" % (quote, lid))
                    response = "Quote added as #%s" % lid

                #remove quote
                if mod == "remove" and args[1].isdigit():
                    print user + " " + admin
                    if user != admin:
                        return "You're not allowed to do that! *blows rape whistle*"
                    id = args[1]
                    query = "DELETE FROM quotes WHERE id = %s" % id
                    cur.execute(query)
                    if cur.rowcount:
                        bot.logger.meta("[Successfully removed quote with id %s]" % id)
                        response = "Successfully removed quote #%s" % id
                    else:
                        response = "I found no quote with that id"

                #search quote by id
                if mod.isdigit():
                    query = "SELECT * FROM quotes WHERE id = %s" % mod
                    cur.execute(query)
                    rows = cur.fetchall()
                    if len(rows) > 0:
                        for row in rows:
                            response = "#%s - %s" % (row[0], row[1])
                    else:
                        response = "I found no quote with that id"                    

                #TODO: search quote by text
                if mod == "search":
                    search = " ".join(args[1:])
                    cur.execute("SELECT * FROM quotes WHERE quote LIKE ?", ('%'+search+'%',))
                    rows = cur.fetchall()
                    if len(rows) > 0:
                        for row in rows:
                            response = "#%s - %s\n" % (row[0], row[1])
                            bot.msg(user, response.encode('utf-8'))
                        return
                    else:
                        response = "I found no quote containing \"%s\"" % search

            else:

                # Get a random quote
                cur.execute("SELECT * FROM Quotes ORDER BY RANDOM() LIMIT 1")
                while True:
                    row = cur.fetchone()
                    if row == None:
                        break
                    print row[1]
                    response = row[1]

        finally:
            if con:
                con.commit()
                con.close()

        if response is not None:
            return response


Action.actions = {
    "!ping": ping,
    "!catfact": CatFacts,
    "!slap": Slap,
    "!urban": UrbanDict,
    # "!tell": Tell,
    "!grab": Grab,
    "!quote": Quote
}